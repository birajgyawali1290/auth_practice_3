import axios from "axios";
import ErrorHttpStatusCode from "../helpers/https-status.code";
const axiosInstance = axios.create({
  baseURL: process.env.REACT_APP_AUTH_URL,
  timeout: 20000,
  timeoutErrorMessage: "Server Time Out",
  headers: {
    "Content-Type": "application/json"
  }
});

axios.interceptors.response.use(
    (response) => {
        return response.data
    },
    (error) => {
        ErrorHttpStatusCode(error)
    }
)
export default axiosInstance;