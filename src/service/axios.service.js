import axiosInstance from "../config/axios.connect";

class HttpService {
    headers = {};
    getHeaders = (config) => {
        if(config.login){
            let token = localStorage.getItem("auth_token");
            this.headers = {
                "authorization": "Bearer "+token,
                "content-type": "application/json"
            }
        }

        if(config.files){
            this.headers = {
                ...this.headers,
                "content-type": "multipart/form-data"
            }
        }
    }

    postRequest = async(url, data, config={}) => {
        try{
            this.getHeaders(config);
            let response = await axiosInstance.post(url, data, {
                headers: this.headers
            })
            return response
        }catch(error){
            console.log("post Request", error)
            throw error
        }
    } 
}
export default HttpService;
