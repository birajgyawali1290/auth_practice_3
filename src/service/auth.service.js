import { toast } from "react-toastify";
import HttpService from "./axios.service";

class AuthService extends HttpService{
    login = async(data) => {
        try{
            let response = await this.postRequest("login", data)
            localStorage.setItem("auth_token", response.data.result.access_token)
            let local_user = {
                name: response.data.result.user.name,
                email: response.data.result.user.email,
                role: response.data.result.user.role,
                user_id: response.data.result.user._id,
            }
            return local_user;
        }catch(error){
            throw error
        }
    }
    register = async(data) => {
        try{
            let response = await this.postRequest('register', data, {files: true})
            if(response.data.status){
                toast.success(response.data.msg)
                return response.data
            }
        }catch(error){
            throw error
        }
    }
}
export const auth_svc = new AuthService()
export default AuthService;