import { StatusCodes } from "http-status-codes"
import { toast } from "react-toastify"
const ErrorHttpStatusCode = (error) => {
    if(error.response.status === StatusCodes.INTERNAL_SERVER_ERROR){
        return toast.error(error.message)
    }else if(error.response.status === StatusCodes.BAD_GATEWAY){
        toast.error(error.response.data.msg)
    }else if(error.response.status === StatusCodes.NOT_FOUND){
        return toast.error(error.message)
    }else if(error.response.status === StatusCodes.UNAUTHORIZED){
        localStorage.removeItem("auth_token")
        return toast.error(error.message)
    }
}

export default ErrorHttpStatusCode;