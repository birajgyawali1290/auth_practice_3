import LoginLayout from "./login.layout"
import RegisterLayout from "./register.layout";
const Layout = {
    LoginLayout,
    RegisterLayout
}
export default Layout;