import * as Yup from "yup";
import { useFormik } from "formik";
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { auth_svc } from "../../service/auth.service";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
const RegisterLayout = () => {
  let navigate = useNavigate();
  let defaultValues = {
    name: "",
    email: "",
    password: "",
    role: "",
    status: "",
    mobile: "",
    address: "",
    image: "",
  };
  const validateValue = Yup.object({
    name: Yup.string()
      .min(3, "Name must contain more than 3 letters")
      .required(),
    email: Yup.string().email().required("Email is required field!"),
    password: Yup.string()
      .min(5, "Password must contain more than 5 characters")
      .required("Password is required field!"),
    role: Yup.string().required("Role is required field!"),
    status: Yup.string().required("Status is required field!"),
    mobile: Yup.number().required("Mobile is required field!"),
    address: Yup.string(),
    image: Yup.string().required("Image is required field!"),
  });
  const formik = useFormik({
    initialValues: defaultValues,
    validationSchema: validateValue,
    onSubmit: (values) => {
      //   console.log("Values", values);
    },
  });
  const imageFilter = (e) => {
    let { files } = e.target;
    let file = files[0];
    let splitFile = file.name.split(".");
    let ext = splitFile.pop();
    let allowed_image = ["jpg", "jpeg", "png", "svg", "webp", "gif", "bmp"];
    if (allowed_image.includes(ext.toLowerCase())) {
      if (file.size <= 5 * 1024 * 1024) {
        formik.setValues({
          ...formik.values,
          image: file,
        });
      } else {
        formik.setErrors({
          ...formik.errors,
          image: "File should be less than 5 mb",
        });
      }
    } else {
      formik.setErrors({
        ...formik.errors,
        image: "File format not supported",
      });
    }
  };
  const handleSubmit = async (e) => {
    try {
      e.preventDefault();
      formik.handleSubmit();
      let values = {
        name: formik.values.name,
        email: formik.values.email,
        password: formik.values.password,
        role: formik.values.role,
        status: formik.values.status,
        mobile: Number(formik.values.mobile),
        address: formik.values.address,
        image: formik.values.image.name,
      };
      let result = await auth_svc.register(values);
      if (result) {
        navigate("/");
      }
    } catch (error) {
      if (error.response?.status === 400) {
        if (error.response?.data?.msg) {
          toast.warning(error.response.data.msg);
        }
      }
      console.log("Register Error", error);
    }
  };
  return (
    <>
      <div className="login-page">
        <Container>
          <Row className="justify-content-center align-items-center">
            <Col md={6}>
              <div className="login-form">
                <h2>Register</h2>
                <Form onSubmit={handleSubmit}>
                  <Form.Group>
                    <Form.Label>Name</Form.Label>
                    <Form.Control
                      name="name"
                      type="text"
                      placeholder="Enter your name..."
                      onChange={formik.handleChange}
                    />
                    <span className="text-danger">{formik.errors?.name}</span>
                  </Form.Group>
                  <Form.Group className="my-3">
                    <Form.Label>Email</Form.Label>
                    <Form.Control
                      name="email"
                      type="email"
                      placeholder="Enter your email..."
                      onChange={formik.handleChange}
                    />
                    <span className="text-danger">{formik.errors?.email}</span>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                      name="password"
                      type="password"
                      placeholder="Enter your password..."
                      onChange={formik.handleChange}
                    />
                    <span className="text-danger">
                      {formik.errors?.password}
                    </span>
                  </Form.Group>
                  <Form.Group className="my-3">
                    <Form.Label>Role</Form.Label>
                    <Form.Select
                      as="select"
                      name="role"
                      onChange={formik.handleChange}
                    >
                      <option value={"" ? formik.errors.role : ""}>
                        --Select one option--
                      </option>
                      <option value={"customer"}>Customer</option>
                      <option value={"seller"}>Seller</option>
                    </Form.Select>
                    <span className="text-danger">{formik.errors?.role}</span>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Status</Form.Label>
                    <Form.Select
                      as="select"
                      name="status"
                      onChange={formik.handleChange}
                    >
                      <option value={"" ? formik.errors.status : ""}>
                        --Select one option--
                      </option>
                      <option value={"active"}>Active</option>
                      <option value={"inactive"}>Inactive</option>
                    </Form.Select>
                    <span className="text-danger">{formik.errors?.status}</span>
                  </Form.Group>
                  <Form.Group className="my-3">
                    <Form.Label>Mobile</Form.Label>
                    <Form.Control
                      name="mobile"
                      type="text"
                      placeholder="Enter your mobile number..."
                      onChange={formik.handleChange}
                    />
                    <span className="text-danger">{formik.errors?.mobile}</span>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Address</Form.Label>
                    <Form.Control
                      name="address"
                      type="text"
                      placeholder="Enter your Address"
                      onChange={formik.handleChange}
                    />
                    <span className="text-danger">
                      {formik.errors?.address}
                    </span>
                  </Form.Group>
                  <Form.Group className="my-3 row">
                    <Form.Label>Image</Form.Label>
                    <Col sm={7}>
                      <Form.Control
                        name="image"
                        type="file"
                        accept="image/*"
                        onChange={imageFilter}
                      />
                    </Col>
                    <Col sm={5}>
                        {
                            formik.values?.image  ?
                            <>
                                <img src={URL.createObjectURL(formik.values?.image)} className="img img-fluid w-50" alt="register_logo"/>
                            </>
                            :
                            "" 
                        }
                    </Col>

                    <span className="text-danger">{formik.errors?.image}</span>
                  </Form.Group>

                  <Button variant="success" type="submit">
                    Register
                  </Button>
                </Form>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};
export default RegisterLayout;
