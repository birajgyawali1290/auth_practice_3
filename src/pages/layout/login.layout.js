import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { useState } from "react";
import "../../assets/css/global.css";
import { NavLink } from "react-router-dom";
import { auth_svc } from "../../service/auth.service";
import { toast } from "react-toastify";
const LoginLayout = () => {
  let [data, setData] = useState({
    email: null,
    password: null,
  });
  const handleChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value,
    });
  };
  const handleSubmit = async (e) => {
    try {
      e.preventDefault();
      let result = await auth_svc.login(data);
      if (result) {
        toast.success("Welcome to " + result.role + " panel");
      }
    } catch (error) {
      if (error.response.status === 400) {
        if(error.response.data.msg){
            toast.error(error.response.data.msg)
        }
      } else {
        console.log("Login Error", error);
      }
    }
  };
  return (
    <>
      <div className="login-page">
        <Container>
          <Row className="justify-content-center align-items-center">
            <Col md={6}>
              <div className="login-form">
                <h2>Login</h2>
                <Form onSubmit={handleSubmit}>
                  <Form.Group>
                    <Form.Label>Email address</Form.Label>
                    <Form.Control
                      name="email"
                      type="email"
                      placeholder="Enter your email..."
                      onChange={handleChange}
                    />
                  </Form.Group>

                  <Form.Group className="my-3">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                      name="password"
                      type="password"
                      placeholder="Enter your password..."
                      onChange={handleChange}
                    />
                  </Form.Group>

                  <Button variant="success" type="submit">
                    Login
                  </Button>
                </Form>
                <NavLink to={"/register"}>Register</NavLink>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};
export default LoginLayout;
